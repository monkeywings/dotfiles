source $HOME/.config/nvim/vim-plug/plugins.vim
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set number
set laststatus=2
let g:vim_markdown_folding_disabled = 1
